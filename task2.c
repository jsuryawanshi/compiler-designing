//opening a intermediate file 

#include<stdio.h>
#include<stdlib.h>

int main()
{
 char ch,file_name[25];
 FILE *fp;

 printf("Enter your file name:");
 scanf("%s",file_name);

 fp=fopen("intermediate.c","r");

 if(fp==NULL)
 {
  printf("Error while opening the file");
  exit(EXIT_FAILURE);
 }
 printf("%s",file_name);
 while((ch=fgetc(fp))!=EOF)
  printf("%c",ch);

 fclose(fp);


 return 0;
}
